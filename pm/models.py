from django.db import models
from django.urls import reverse



class Project(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('project_list')

class TC(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    project = models.ForeignKey( Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('tc_new')


class TCO(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    tc = models.ForeignKey( TC, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tco_new')


class TRE(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    tco = models.ForeignKey( TCO, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tre_new')


class TRF(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    tco = models.ForeignKey( TCO, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('trf_new')
