from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy
from . import models

# Create your views here.

# views project

class ProjectListView(ListView):
    model = models.Project
    template_name = 'project_list.html'


class PorjectCreateView(CreateView):
    model = models.Project
    template_name = 'project_new.html'
    success_url = reverse_lazy('project_list')
    fields = '__all__'

class ProjectEditView(UpdateView):
    model = models.Project
    template_name = 'project_edit.html'
    fields = '__all__'

# create list views task client

class TcCreateView(CreateView):
    model = models.TC
    template_name = 'tc_new.html'
    fields = '__all__'

class TcListView(ListView):
    model = models.TC
    template_name = 'tc_list.html'

#create list views TCO

class TcoCreateView(CreateView):
    model = models.TCO
    template_name = 'tco_new.html'
    fields = '__all__'

class TcoListView(ListView):
    model = models.TCO
    template_name = 'tco_list.html'

#create list views TRE

class TreCreateView(CreateView):
    model = models.TRE
    template_name = 'tre_new.html'
    fields = '__all__'

class TreListView(ListView):
    model = models.TCO
    template_name = 'tre_list.html'

#create list views TRF

class TrfCreateView(CreateView):
    model = models.TRF
    template_name = 'trf_new.html'
    fields = '__all__'

class TrfListView(ListView):
    model = models.TRF
    template_name = 'trf_list.html'