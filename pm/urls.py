from . import views
from django.urls import path


urlpatterns =[
    path('project/new', views.PorjectCreateView.as_view(), name='project_new'),
    path('project/list/', views.ProjectListView.as_view(), name='project_list'),
    path('project/edit/<int:pk>/', views.ProjectEditView.as_view(), name='project_edit'),
    path('tc/new', views.TcCreateView.as_view(), name='tc_new'),
    path('tco/new/', views.TcoCreateView.as_view(), name='tco_new'),
    path('tre/new/', views.TreCreateView.as_view(), name='tre_new'),
    path('trf/new/', views.TrfCreateView.as_view(), name='trf_new'),
]